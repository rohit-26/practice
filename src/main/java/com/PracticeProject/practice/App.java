package com.PracticeProject.practice;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
       App a= new App();
       int [] num= {1,3,5,6};
       int check=a.searchInsert(num, 2);
       System.out.println("Checking "+check);
    }
    
    
    public int searchInsert(int[] nums, int target) {
        int result = 0;
       
       for (int i = 0; i < nums.length; i++) {
           if (nums[i] == target) {
               result = i;
               break;
           } else if (nums[i] < target) {
               result++;
           }
           
       
       }
	return result;
   }
}
